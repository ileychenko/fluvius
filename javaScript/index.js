(function () {


    const list = [10, 20, 30];

    function changeElements(list) {
        const tmp = list.concat();
        list[0] = tmp[1];
        list[1] = tmp[0];
    }

    changeElements(list);
    console.log(list); // [20, 10, 30];
})();

(function () {


    const list = [30, -5, 0, 10, 5];

    function mySort(list) {
        let tmp = list.concat();
        for (let id = 0; list.length > id; id++) {
            let resultId = 0;
            if(tmp.length > 1) {
                for (let tmp_id = 1; tmp.length > tmp_id; tmp_id++) {
                    if (tmp[resultId] > tmp[tmp_id]) {
                        resultId = tmp_id;
                    }
                }
            }
            list[id] = tmp[resultId];
            tmp.splice(resultId, 1);
        }

    }

    mySort(list);

    console.log(list); // [-5, 0, 5, 10, 30]

})();
(function () {
    const alert = function (message) {
        console.log(message)
    };

    var func1 = function(message) {
        this(message);
    }

    var func2 = func1.bind(alert);
    func2('Test'); // alert 'Test'

    function myBind(func, context) {
        return function () {
            func.apply(context,arguments);
        };
    }

    var func3 = myBind(func1, alert);
    func3('Test'); // alert 'Test'
})()