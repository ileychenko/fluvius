import * as Bluebird from 'bluebird'
export interface process {
    env: {
        NODE_ENV: string
        UV_THREADPOOL_SIZE: number
        NODE_APP_INSTANCE: string
        API_VERSION: number
    }
    exit: Function
}


declare module "*.json" {
    const value: any;
    export default value;
}