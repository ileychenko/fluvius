import path = require('path');
import * as dotenv from 'dotenv';

dotenv.config({path: path.join(process.cwd(), process.env.NODE_ENV == 'production' ? 'env.prod' : 'env.dev')});

import express = require('express');
import http = require('http');
import cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
import {router} from './app/decorators/routedecorators'
import {getRoutes} from './initializers/routes'

mongoose.Promise = Promise;
mongoose.connect(process.env.URL);

getRoutes();

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

app.use('/v1', router);


// catch 404 and forward to error handler
app.use(function (req, res) {
    res.sendStatus(404).send();
});


http.createServer(app).listen(process.env.PORT ? process.env.PORT : process.env.LOCAL_PORT, () => {
    console.log(`Node app is running on port SERVER  ${process.env.PORT ? process.env.PORT : process.env.LOCAL_PORT}`);
});


