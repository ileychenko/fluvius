"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const dotenv = require("dotenv");
dotenv.config({ path: path.join(process.cwd(), process.env.NODE_ENV == 'production' ? 'env.prod' : 'env.dev') });
const express = require("express");
const http = require("http");
const cookieParser = require("cookie-parser");
const mongoose = require('mongoose');
const routedecorators_1 = require("./app/decorators/routedecorators");
const routes_1 = require("./initializers/routes");
mongoose.Promise = Promise;
mongoose.connect(process.env.URL);
routes_1.getRoutes();
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/v1', routedecorators_1.router);
app.use(function (req, res) {
    res.sendStatus(404).send();
});
http.createServer(app).listen(process.env.PORT ? process.env.PORT : process.env.LOCAL_PORT, () => {
    console.log(`Node app is running on port SERVER  ${process.env.PORT ? process.env.PORT : process.env.LOCAL_PORT}`);
});
//# sourceMappingURL=app.js.map