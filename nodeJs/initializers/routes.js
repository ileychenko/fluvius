"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
function getRoutes() {
    exports.Controllers = exports.FindFilesSync(exports.ResolvePath('app'), '.controller.js')
        .filter(e => !e.includes('map'))
        .map(prepareModule);
}
exports.getRoutes = getRoutes;
exports.FindFilesSync = (startPath, filter) => {
    const fileList = [];
    const recurse = (startPath, filter) => {
        fs_1.readdirSync(startPath).map((file) => {
            const filename = path_1.join(startPath, file);
            const stat = fs_1.lstatSync(filename);
            if (stat.isDirectory())
                recurse(filename, filter);
            else if (filename.includes(filter))
                fileList.push(filename);
        });
    };
    recurse(startPath, filter);
    return fileList;
};
function prepareModule(file_path) {
    console.log('Load controller: ' + file_path);
    return {
        file: path_1.basename(file_path),
        module: require(file_path)
    };
}
exports.ResolvePath = (...args) => path_1.resolve(...args);
//# sourceMappingURL=routes.js.map