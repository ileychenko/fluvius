import {readdirSync, lstatSync} from 'fs'
import { join, basename, resolve } from 'path'
export let Controllers: any;

/**
 * Run init routes
 */
export function getRoutes() {
    Controllers = FindFilesSync(ResolvePath('app'), '.controller.js')
        .filter(e=> !e.includes('map'))
        .map(prepareModule)

}

/**
 * Get files by filter
 * @param {string} startPath
 * @param {string} filter
 * @returns {string[]}
 * @constructor
 */
export const FindFilesSync = (startPath: string, filter: string): string[] => {
    const fileList: string[] = [];
    const recurse = (startPath: string, filter: string) => {
        readdirSync(startPath).map((file) => {
            const filename: string = join(startPath, file);
            const stat = lstatSync(filename);
            if (stat.isDirectory()) recurse(filename, filter);
            else if (filename.includes(filter)) fileList.push(filename)
        })
    };
    recurse(startPath, filter);
    return fileList
};

/**
 * Get and Load modules
 * @param {string} file_path
 * @returns {{file: string; module: any}}
 */
function prepareModule(file_path: string) {
    console.log('Load controller: '+file_path);
    return {
        file: basename(file_path),
        module: require(file_path)
    }
}

/**
 * Resolve path
 * @param {string} args
 * @constructor
 */
export const ResolvePath = (...args:string[]) => resolve(...args);