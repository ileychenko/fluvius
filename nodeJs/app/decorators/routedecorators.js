"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
exports.router = express.Router();
function Get(routeUrl, ...functions) {
    return DC('get', routeUrl, functions);
}
exports.Get = Get;
function Post(routeUrl, ...functions) {
    return DC('post', routeUrl, functions);
}
exports.Post = Post;
function Put(routeUrl, ...functions) {
    return DC('put', routeUrl, functions);
}
exports.Put = Put;
function Delete(routeUrl, ...functions) {
    return DC('delete', routeUrl, functions);
}
exports.Delete = Delete;
function All(routeUrl, ...functions) {
    return DC('all', routeUrl, functions);
}
exports.All = All;
function addToRoute(metod, routeUrl, handler, functions = []) {
    switch (metod) {
        case 'get': {
            functions.push(handler);
            exports.router.get(routeUrl, ...functions);
            break;
        }
        case 'post': {
            functions.push(handler);
            exports.router.post(routeUrl, ...functions);
            break;
        }
        case 'put': {
            functions.push(handler);
            exports.router.put(routeUrl, ...functions);
            break;
        }
        case 'delete': {
            functions.push(handler);
            exports.router.delete(routeUrl, ...functions);
            break;
        }
        default: {
            functions.push(handler);
            exports.router.all(routeUrl, ...functions);
        }
    }
}
function DC(metod, routeUrl, functions = []) {
    return function DcRoute(target, propertyKey, descriptor) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function (req, res) {
            try {
                const result = await originalDescriptor.call(this, req, res);
                res.json({ data: result }).send();
            }
            catch (e) {
                console.log(e);
                if (e.code) {
                    res.status(e.code).send(e.code);
                }
                else {
                    res.status(500).send(e);
                }
            }
        };
        addToRoute(metod, routeUrl, descriptor.value, functions);
        return descriptor;
    };
}
function Params(...args) {
    return function DcRoute(target, propertyKey, descriptor) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function DcDsExtractParams(...values) {
            const req = values[values.length - 2];
            return await originalDescriptor.call(this, ...(values.filter((elm, key) => key < values.length - 2)), ...(args.map(elm => elm in req.params ? req.params[elm] : null)), ...(values.filter((elm, key) => key >= values.length - 2)));
        };
        Object.defineProperty(descriptor.value, 'name', {
            value: `${descriptor.value.name}:${originalDescriptor.name}`,
            writable: false
        });
        return descriptor;
    };
}
exports.Params = Params;
function Body(...args) {
    return function DcRoute(target, propertyKey, descriptor) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function DcDsExtractParams(...values) {
            const req = values[values.length - 2];
            return await originalDescriptor.call(this, ...(values.filter((elm, key) => key < values.length - 2)), ...(args.map(elm => elm in req.body ? req.body[elm] : null)), ...(values.filter((elm, key) => key >= values.length - 2)));
        };
        Object.defineProperty(descriptor.value, 'name', {
            value: `${descriptor.value.name}:${originalDescriptor.name}`,
            writable: false
        });
        return descriptor;
    };
}
exports.Body = Body;
function Query(...args) {
    return function DcRoute(target, propertyKey, descriptor) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function DcDsExtractParams(...values) {
            const req = values[values.length - 2];
            return await originalDescriptor.call(this, ...(values.filter((elm, key) => key < values.length - 2)), ...(args.map(elm => elm in req.query ? req.query[elm] : null)), ...(values.filter((elm, key) => key >= values.length - 2)));
        };
        Object.defineProperty(descriptor.value, 'name', {
            value: `${descriptor.value.name}:${originalDescriptor.name}`,
            writable: false
        });
        return descriptor;
    };
}
exports.Query = Query;
//# sourceMappingURL=routedecorators.js.map