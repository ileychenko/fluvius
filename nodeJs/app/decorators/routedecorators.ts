import express = require('express');

export const router: express.Router = express.Router();

/**
 * Decorator, add function to route by GET metods
 * @param {string} routeUrl
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Get(routeUrl: string, ...functions: Function[]) {
    return DC('get', routeUrl, functions);
}

/**
 * Decorator, add function to route by POST metod
 * @param {string} routeUrl
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Post(routeUrl: string, ...functions: Function[]) {
    return DC('post', routeUrl, functions);
}

/**
 * Decorator, add function to route by PUT metod
 * @param {string} routeUrl
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Put(routeUrl: string, ...functions: Function[]) {
    return DC('put', routeUrl, functions);
}

/**
 * Decorator, add function to route by GET metod
 * @param {string} routeUrl
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Delete(routeUrl: string, ...functions: Function[]) {
    return DC('delete', routeUrl, functions);
}

/**
 * Decorator, add function to route by ALL metods
 * @param {string} routeUrl
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function All(routeUrl: string, ...functions: Function[]) {
    return DC('all', routeUrl, functions);
}

/**
 * Add handler to route by metod
 * @param {string} metod
 * @param {string} routeUrl
 * @param handler
 */
function addToRoute(metod: string, routeUrl: string, handler: any, functions: any[] = []) {
    switch (metod) {
        case 'get' : {
            functions.push(handler);
            router.get(routeUrl, ...functions);
            break;
        }
        case 'post' : {
            functions.push(handler);
            router.post(routeUrl, ...functions);
            break;
        }
        case 'put' : {
            functions.push(handler);
            router.put(routeUrl, ...functions);
            break;
        }
        case 'delete' : {
            functions.push(handler);
            router.delete(routeUrl, ...functions);
            break;
        }
        default : {
            functions.push(handler);
            router.all(routeUrl, ...functions);
        }
    }
}

/**
 * Init DC function by route
 * @param {string} metod
 * @param {string} routeUrl
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
function DC(metod: string, routeUrl: string, functions: Function[] = []) {
    return function DcRoute(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
        const originalDescriptor = descriptor.value;

        descriptor.value = async function (req: express.Request, res: express.Response) {
            try {
                const result = await originalDescriptor.call(this, req, res);
                res.json({data: result}).send();
            } catch (e) {
                console.log(e)
                if( e.code ) {
                    res.status(e.code).send(e.code);
                } else {
                    res.status(500).send(e);
                }

            }
        };
        addToRoute(metod, routeUrl, descriptor.value, functions);
        return descriptor;
    }
}

/**
 * Decorator function get req.params
 * @param {string} args
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Params(...args: string[]) {
    return function DcRoute(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function DcDsExtractParams(...values: any[]) {
            const req = values[values.length - 2];
            return await originalDescriptor.call(this,
                ...(values.filter((elm, key) => key < values.length - 2)),
                ...(args.map(elm => elm in req.params ? req.params[elm] : null)),
                ...(values.filter((elm, key) => key >= values.length - 2)));
        };
        Object.defineProperty(descriptor.value, 'name', {
            value: `${descriptor.value.name}:${originalDescriptor.name}`,
            writable: false
        });
        return descriptor;
    }
}

/**
 * Decorator function get req.body
 * @param {string} args
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Body(...args: string[]) {
    return function DcRoute(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function DcDsExtractParams(...values: any[]) {
            const req = values[values.length - 2];
            return await originalDescriptor.call(this,
                ...(values.filter((elm, key) => key < values.length - 2)),
                ...(args.map(elm => elm in req.body ? req.body[elm] : null)),
                ...(values.filter((elm, key) => key >= values.length - 2))
            );
        };
        Object.defineProperty(descriptor.value, 'name', {
            value: `${descriptor.value.name}:${originalDescriptor.name}`,
            writable: false
        });
        return descriptor;
    }
}

/**
 * Decorator function get req.query
 * @param {string} args
 * @returns {(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>}
 * @constructor
 */
export function Query(...args: string[]) {
    return function DcRoute(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
        const originalDescriptor = descriptor.value;
        descriptor.value = async function DcDsExtractParams(...values: any[]) {
            const req = values[values.length - 2];
            return await originalDescriptor.call(this,
                ...(values.filter((elm, key) => key < values.length - 2)),
                ...(args.map(elm => elm in req.query ? req.query[elm] : null)),
                ...(values.filter((elm, key) => key >= values.length - 2))
            );
        };
        Object.defineProperty(descriptor.value, 'name', {
            value: `${descriptor.value.name}:${originalDescriptor.name}`,
            writable: false
        });
        return descriptor;
    }
}