"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const uuidV4 = require('uuid/v4');
const EmailValidator = require("email-validator");
const users_1 = require("../../base/models/users");
const tokens_1 = require("../../base/models/tokens");
const routedecorators_1 = require("../../decorators/routedecorators");
class AuthController {
    async logout(token) {
        return Promise.resolve();
    }
    async login(email, password) {
        if (EmailValidator.validate(email)) {
            const authLogin = new users_1.AuthLogin(email, password);
            const user = await users_1.dbUsers.findUsersCollection(authLogin);
            if (user) {
                const token = uuidV4();
                const tokenCol = await tokens_1.dbToken.findTokenByUserCollection(user.uuid);
                if (tokenCol) {
                    await tokens_1.dbToken.updateTokenCollection(tokenCol, token);
                }
                else {
                    await tokens_1.dbToken.addTokenCollection(user.uuid, token);
                }
                return Promise.resolve({
                    tocken: token,
                    user: user
                });
            }
            else {
                return Promise.reject({ code: 400 });
            }
        }
        else {
            return Promise.reject({ code: 400 });
        }
    }
    async create(email, password) {
        if (EmailValidator.validate(email)) {
            const authLogin = new users_1.AuthLogin(email, password);
            const user = await users_1.dbUsers.addUsersCollection(authLogin);
            if (user) {
                return Promise.resolve({
                    tocken: uuidV4(),
                    user: user
                });
            }
            else {
                return Promise.reject({ code: 400 });
            }
        }
        else {
            return Promise.reject({ code: 400 });
        }
    }
}
tslib_1.__decorate([
    routedecorators_1.Delete('/auth/logout'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], AuthController.prototype, "logout", null);
tslib_1.__decorate([
    routedecorators_1.Post('/auth/login'),
    routedecorators_1.Body('email', 'password'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
tslib_1.__decorate([
    routedecorators_1.Post('/auth/create'),
    routedecorators_1.Body('email', 'password'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], AuthController.prototype, "create", null);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map