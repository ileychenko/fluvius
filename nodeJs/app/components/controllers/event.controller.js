"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const event_1 = require("../../base/models/event");
const auth_1 = require("../../libs/auth");
const routedecorators_1 = require("../../decorators/routedecorators");
class EventController {
    async insert(name, date) {
        const parceDate = Date.parse(date);
        if (!name || !date || isNaN(parceDate) != false) {
            return Promise.reject({ code: 400 });
        }
        else {
            const newEvent = await event_1.dbEvent.addEventCollection(name, new Date(date));
            return Promise.resolve(newEvent);
        }
    }
    async get(id) {
        const event = await event_1.dbEvent.findEventCollection(id);
        return Promise.resolve(event);
    }
    async getList(count, page, sortdate) {
        const eventList = await event_1.dbEvent
            .listEventCollection(Number(count), Number(page), sortdate);
        return Promise.resolve(eventList);
    }
    async update(id, name, date) {
        const parceDate = Date.parse(date);
        if (!id || !name || !date || isNaN(parceDate) != false) {
            return Promise.reject({ code: 400 });
        }
        else {
            let event = await event_1.dbEvent.findEventCollection(id);
            if (event) {
                event_1.dbEvent.updateEventCollection(id, name, new Date(date));
                event = await event_1.dbEvent.findEventCollection(id);
                return Promise.resolve(event);
            }
            else {
                return Promise.reject({ code: 400 });
            }
        }
    }
    async delete(id) {
        let event = await event_1.dbEvent.findEventCollection(id);
        if (event) {
            await event_1.dbEvent.removeEventCollection(id);
            return Promise.resolve();
        }
        else {
            return Promise.reject({ code: 400 });
        }
    }
}
tslib_1.__decorate([
    routedecorators_1.Post('/events/insert', auth_1.AuthAnyRequest),
    routedecorators_1.Body('name', 'date'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], EventController.prototype, "insert", null);
tslib_1.__decorate([
    routedecorators_1.Get('/events/get/:id', auth_1.AuthAnyRequest),
    routedecorators_1.Params('id'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], EventController.prototype, "get", null);
tslib_1.__decorate([
    routedecorators_1.Get('/events/list', auth_1.AuthAnyRequest),
    routedecorators_1.Query('count', 'page', 'sortdate'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], EventController.prototype, "getList", null);
tslib_1.__decorate([
    routedecorators_1.Put('/events/update/:id', auth_1.AuthAnyRequest),
    routedecorators_1.Params('id'),
    routedecorators_1.Body('name', 'date'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], EventController.prototype, "update", null);
tslib_1.__decorate([
    routedecorators_1.Delete('/events/delete/:id', auth_1.AuthAnyRequest),
    routedecorators_1.Params('id'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], EventController.prototype, "delete", null);
exports.EventController = EventController;
//# sourceMappingURL=event.controller.js.map