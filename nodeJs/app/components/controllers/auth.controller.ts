const uuidV4 = require('uuid/v4');
import * as EmailValidator from 'email-validator';
import {AuthLogin, dbUsers} from '../../base/models/users'
import {dbToken} from '../../base/models/tokens'

import {
    // Get,
    Post,
    Body,
    Delete
    // Params
} from '../../decorators/routedecorators'

/**
 * class route cmd
 */
export class AuthController {

    /**
     * Logout
     * @param {string} token
     * @returns {Promise<void>}
     */
    @Delete('/auth/logout')
    async logout(token: string) {
        return Promise.resolve();
    }

    /**
     * Login
     * @param {string} email
     * @param {string} password
     * @returns {Promise<{tocken: string}>}
     */
    @Post('/auth/login')
    @Body('email', 'password')
    async login(email: string, password: string) {
        if (EmailValidator.validate(email)) {
            const authLogin = new AuthLogin(email, password);
            const user = await dbUsers.findUsersCollection(authLogin);
            if (user) {
                const token = uuidV4();
                const tokenCol = await dbToken.findTokenByUserCollection(user.uuid);
                if (tokenCol) {
                    await dbToken.updateTokenCollection(tokenCol, token);
                } else {
                    await dbToken.addTokenCollection(user.uuid, token);
                }

                return Promise.resolve({
                    tocken: token,
                    user: user
                });
            } else {
                return Promise.reject({code: 400})
            }
        } else {
            return Promise.reject({code: 400})
        }

    }

    /**
     * create user
     * @param {string} email
     * @param {string} password
     * @returns {Promise<{tocken: string}>}
     */
    @Post('/auth/create')
    @Body('email', 'password')
    async create(email: string, password: string) {
        if (EmailValidator.validate(email)) {
            const authLogin = new AuthLogin(email, password);
            const user = await dbUsers.addUsersCollection(authLogin)
            if (user) {


                return Promise.resolve({
                    tocken: uuidV4(),
                    user: user
                });
            } else {
                return Promise.reject({code: 400})
            }
        } else {
            return Promise.reject({code: 400})
        }
    }
}