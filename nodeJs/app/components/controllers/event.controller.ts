import {dbEvent} from '../../base/models/event'
import {AuthAnyRequest} from '../../libs/auth'

import {
    Get,
    Post,
    Body,
    Put,
    Delete,
    Params,
    Query,
} from '../../decorators/routedecorators'

/**
 * class route cmd
 */
export class EventController {

    /**
     * Insert
     * @param {string} name
     * @param {Date} date
     * @returns {Promise<*>}
     */
    @Post('/events/insert', AuthAnyRequest)
    @Body('name', 'date')
    async insert(name: string, date: string) {
        const parceDate = Date.parse(date);
        if (!name || !date || isNaN(parceDate) != false) {
            return Promise.reject({code: 400});
        } else {
            const newEvent = await dbEvent.addEventCollection(name, new Date(date))
            return Promise.resolve(newEvent);
        }

    }

    /**
     * get by id
     * @param {string} name
     * @param {Date} date
     * @returns {Promise<*>}
     */
    @Get('/events/get/:id', AuthAnyRequest)
    @Params('id')
    async get(id: string) {
        const event = await dbEvent.findEventCollection(id)
        return Promise.resolve(event);
    }

    /**
     * Get List
     * @param {string} count
     * @param {string} page
     * @param {string} sortdate  asc or desc
     * @returns {Promise<any>}
     */
    @Get('/events/list', AuthAnyRequest)
    @Query('count', 'page', 'sortdate')
    async getList(count: string, page: string, sortdate: string) {
        const eventList = await dbEvent
            .listEventCollection(Number(count), Number(page), sortdate)
        return Promise.resolve(eventList);
    }
    /**
     * Update
     * @param {string} id
     * @param {string} name
     * @param {Date} date
     * @returns {Promise<any>}
     */
    @Put('/events/update/:id', AuthAnyRequest)
    @Params('id')
    @Body('name', 'date')
    async update(id: string, name: string, date: string) {
        const parceDate = Date.parse(date);
        if (!id || !name || !date || isNaN(parceDate) != false) {
            return Promise.reject({code: 400});
        } else {
            let event = await dbEvent.findEventCollection(id);
            if (event) {
                dbEvent.updateEventCollection(id, name, new Date(date));
                event = await dbEvent.findEventCollection(id)
                return Promise.resolve(event);
            } else {
                return Promise.reject({code: 400})
            }
        }
    }

    /**
     * Delete
     * @param {string} id
     * @returns {Promise<any>}
     */
    @Delete('/events/delete/:id', AuthAnyRequest)
    @Params('id')
    async delete(id: string) {
        let event = await dbEvent.findEventCollection(id);
        if (event) {
            await dbEvent.removeEventCollection(id);
            return Promise.resolve();
        } else {
            return Promise.reject({code: 400})
        }
    }
}