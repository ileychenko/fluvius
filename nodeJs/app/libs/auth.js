"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tokens_1 = require("../base/models/tokens");
async function AuthAnyRequest(req, res, next) {
    if (req.headers.authorization) {
        const token = await tokens_1.dbToken.findTokenCollection(req.headers.authorization);
        if (token) {
            next();
        }
        else {
            res.sendStatus(401).send();
        }
    }
    else {
        res.sendStatus(401).send();
    }
}
exports.AuthAnyRequest = AuthAnyRequest;
//# sourceMappingURL=auth.js.map