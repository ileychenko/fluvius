import express = require('express');
import {dbToken} from '../base/models/tokens'

export async function AuthAnyRequest(req: any, res: express.Response, next: express.NextFunction) {
    if (req.headers.authorization) {
        const token = await dbToken.findTokenCollection( req.headers.authorization )
        if (token) {
            next();
        } else {
            res.sendStatus(401).send();
        }
    } else {
        res.sendStatus(401).send();
    }
}