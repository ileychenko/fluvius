"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
class DbToken {
    constructor() {
        this.schema = new Schema({
            uuid_user: { type: String, required: true },
            uuid_token: { type: String, required: true }
        });
        this.dbTokenModel = mongoose.model('tokens', this.schema);
    }
    async findTokenCollection(uuid_token) {
        return await this.dbTokenModel
            .findOne({
            uuid_token: uuid_token
        });
    }
    async findTokenByUserCollection(uuid_user) {
        return await this.dbTokenModel
            .findOne({
            uuid_user: uuid_user
        });
    }
    async addTokenCollection(uuid_user, uuid_token) {
        const newToken = new this.dbTokenModel({
            uuid_user: uuid_user,
            uuid_token: uuid_token,
        });
        return await newToken.save();
    }
    async updateTokenCollection(dbToken, uuid_token) {
        dbToken.uuid_token = uuid_token;
        return await dbToken.save();
    }
}
exports.DbToken = DbToken;
exports.dbToken = new DbToken();
//# sourceMappingURL=tokens.js.map