const mongoose = require('mongoose');
const Schema = mongoose.Schema;

export class DbEvent {
    private schema: any;
    public dbEventModel: any
    constructor() {
        this.schema = new Schema({
            name: {type: String, required: true},
            date: {type: Date, required: true}
        });

        this.dbEventModel = mongoose.model('events', this.schema);
    }

    async findEventCollection(id: string){
        return await this.dbEventModel
            .findOne({
                _id : id
            });
    }


    async listEventCollection(count: number = 0, page: number = 0, sortdate: string = ''){
         let prom = this.dbEventModel.find();
         if (count && page) {
             prom = prom.skip(count * (page-1)).limit(count)
         }
         if (sortdate) {
             prom = prom.sort({date: sortdate=='asc'?1:-1})
         }
        return await prom;
    }

    async addEventCollection(name: string, date: Date) {
        const newEvent = new this.dbEventModel({
            name : name,
            date: date
        });
        return await newEvent.save();
    }

    async updateEventCollection(id: string, name: string, date: Date) {
        return await this.dbEventModel.update({_id : id},{
            name: name,
            date: date
        });
    }
    async removeEventCollection(id: string) {
        return await this.dbEventModel.remove({_id : id});
    }
}

export const dbEvent = new DbEvent();