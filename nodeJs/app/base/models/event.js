"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
class DbEvent {
    constructor() {
        this.schema = new Schema({
            name: { type: String, required: true },
            date: { type: Date, required: true }
        });
        this.dbEventModel = mongoose.model('events', this.schema);
    }
    async findEventCollection(id) {
        return await this.dbEventModel
            .findOne({
            _id: id
        });
    }
    async listEventCollection(count = 0, page = 0, sortdate = '') {
        let prom = this.dbEventModel.find();
        if (count && page) {
            prom = prom.skip(count * (page - 1)).limit(count);
        }
        if (sortdate) {
            prom = prom.sort({ date: sortdate == 'asc' ? 1 : -1 });
        }
        return await prom;
    }
    async addEventCollection(name, date) {
        const newEvent = new this.dbEventModel({
            name: name,
            date: date
        });
        return await newEvent.save();
    }
    async updateEventCollection(id, name, date) {
        return await this.dbEventModel.update({ _id: id }, {
            name: name,
            date: date
        });
    }
    async removeEventCollection(id) {
        return await this.dbEventModel.remove({ _id: id });
    }
}
exports.DbEvent = DbEvent;
exports.dbEvent = new DbEvent();
//# sourceMappingURL=event.js.map