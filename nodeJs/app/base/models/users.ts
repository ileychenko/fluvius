const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuidV4 = require('uuid/v4');

export class AuthLogin {
    constructor(
        public email: string = '',
        public password: string = ''
    ) {

    }
}

export class DbUsers {
    private schema: any;
    public dbUserModel: any
    constructor() {
        this.schema = new Schema({
            email: {type: String, required: true},
            password: {type: String, required: true},
            uuid: {type: String, required: true}
        });

        this.dbUserModel = mongoose.model('users', this.schema);
    }

    async findUsersCollection(authLogin: AuthLogin){
        return await this.dbUserModel
            .findOne({
                email : authLogin.email,
                password: authLogin.password
            });
    }

    async addUsersCollection(authLogin: AuthLogin) {
        const newUser = new this.dbUserModel({
            email : authLogin.email,
            password: authLogin.password,
            uuid: uuidV4()
        });
        return await newUser.save();
    }
}

export const dbUsers = new DbUsers();