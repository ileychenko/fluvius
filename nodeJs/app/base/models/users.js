"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuidV4 = require('uuid/v4');
class AuthLogin {
    constructor(email = '', password = '') {
        this.email = email;
        this.password = password;
    }
}
exports.AuthLogin = AuthLogin;
class DbUsers {
    constructor() {
        this.schema = new Schema({
            email: { type: String, required: true },
            password: { type: String, required: true },
            uuid: { type: String, required: true }
        });
        this.dbUserModel = mongoose.model('users', this.schema);
    }
    async findUsersCollection(authLogin) {
        return await this.dbUserModel
            .findOne({
            email: authLogin.email,
            password: authLogin.password
        });
    }
    async addUsersCollection(authLogin) {
        const newUser = new this.dbUserModel({
            email: authLogin.email,
            password: authLogin.password,
            uuid: uuidV4()
        });
        return await newUser.save();
    }
}
exports.DbUsers = DbUsers;
exports.dbUsers = new DbUsers();
//# sourceMappingURL=users.js.map