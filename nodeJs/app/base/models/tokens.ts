const mongoose = require('mongoose');
const Schema = mongoose.Schema;

export class DbToken {
    private schema: any;
    public dbTokenModel: any
    constructor() {
        this.schema = new Schema({
            uuid_user: {type: String, required: true},
            uuid_token: {type: String, required: true}
        });

        this.dbTokenModel = mongoose.model('tokens', this.schema);
    }

    async findTokenCollection(uuid_token: string){
        return await this.dbTokenModel
            .findOne({
                uuid_token : uuid_token
            });
    }
    async findTokenByUserCollection(uuid_user: string){
        return await this.dbTokenModel
            .findOne({
                uuid_user : uuid_user
            });
    }

    async addTokenCollection(uuid_user: string, uuid_token: string) {
        const newToken = new this.dbTokenModel({
            uuid_user : uuid_user,
            uuid_token: uuid_token,
        });
        return await newToken.save();
    }
    async updateTokenCollection(dbToken: any, uuid_token: string) {
        dbToken.uuid_token = uuid_token;
        return await dbToken.save();
    }
}

export const dbToken = new DbToken();